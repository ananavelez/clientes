const express = require('express')
const app = express()
const port = 3000

var cors = require('cors');

app.use(cors())


app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))

var grupo_clientes = [
    {
       "ClienteId":"176",
       "Nombre":"Mauricia Lopez Lopez Casta",
       "Rut":"74801553",
       "Telefono":"5557409",
       "Correo":"mauricio@gmail.com"
    },
    {
       "ClienteId":"184",
       "Nombre":"Carlos Flores",
       "Rut":"88985555",
       "Telefono":"944457896",
       "Correo":"carlos@gmail.com"
    },
    {
       "ClienteId":"185",
       "Nombre":"Miriam Lopez",
       "Rut":"52245454",
       "Telefono":"977841489",
       "Correo":"miriam@gmail.com"
    },
    {
       "ClienteId":"186",
       "Nombre":"Juan Perez",
       "Rut":"444444444",
       "Telefono":"454654654",
       "Correo":"juan@gmail.com"
    }
 ];


 app.get('/api', (req, res) => res.send(grupo_clientes))
 
console.log(grupo_clientes);